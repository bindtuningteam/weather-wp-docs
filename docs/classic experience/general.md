On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.
 
- [Provider Options](./options.md)
- [Layout Settings](./layout.md)
- [Advanced Options](./advanced.md)
- [Web Part Appearance](./appearance.md)
- [Web Part Messages](./message.md)

![Settings](../images/classic/07.webpartproperties.png)

The global settings form let you apply options to **all the web parts** on the page at once. To use the form, follow the steps:

- [Configure Global Settings](./globalsettings.md)

![global_01_tab.PNG](https://bitbucket.org/repo/zpBnoa/images/2405439960-global_01_tab.PNG)