### Language
 
From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).

--------
### Ignore theme font

Enable this option to use the font styles packaged in the web part. Uncheck to use the same font styling as in your pages.

![title](../images/classic/13.advanced.png)