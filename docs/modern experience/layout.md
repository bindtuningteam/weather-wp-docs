### Web Part Layout

In this setting you can select the styling for your web part. Currently 4 layouts are available.

|**Simple Layout**|**Wide Layout**|
|-|-|
|![SimpleLayout.png](../images/classic/14.simplelayout.png)|![WideLayout.png](../images/classic/15.widelayout.png)|

|**Bold Layout**|**Tiny Layout**|
|-|-|
|![BoldLayout.png](../images/classic/16.boldlayout.png)|![tinyLayout.png](../images/classic/17.tinylayout.png)|


----

### Use Background Image

If checked, the web part will display an image appropriate to the current weather conditions of the selected location. If left unchecked, the web part will simply display a white background.
 
----

### Pressure, Humidity and Wind


This option toggles the component that displays extra information regarding today's conditions

----

### Forecast for the week

This options toggles the component that displays the forecast for the next 4 to 6 days, depending on the provider being used

---

### Detect my location

If you intend to have the **Weather** of your current location, you need to enable that option on the Web Part. For that click on **Change Location** dropdown and click on **Detect My Location**. Otherwise, you can set the specific **Weather** that you intend to see on the page by typing the name of the city.

![tinyLayout.PNG](../images/modern/11.detectmylocation.png)

<p class="alert alert-info">Note: You will be requested from the Browser to allow to detect your location.</p>

----

### Ignore theme font

Enable this option to use the font styles packaged in the web part. Uncheck to use the same font styling as in your pages.

----

### Default Unit

Select if you intend to show temperatures using Imperial (Fahrenheit) or Metric (Celsius) units by default.

<p class="alert alert-info">By clicking the <b>C</b> or <b>F</b> buttons on the Web Part content area, users can see temperature based on their selection.</p>

![tinyLayout.PNG](../images/modern/10.layoutoptions.png)
