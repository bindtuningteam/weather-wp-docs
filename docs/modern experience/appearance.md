### Theme Colors on the Title Bar

Enabling this option will make it so the web part's title bar inherits the background color from your theme or customization on the page. 
This option should help in maintaining branding consistency with native SharePoint web parts.

---- 

### Theme Colors on the WebPart Content

Apply the color of the theme to the weather styles Web Part.

![01.appearance.color.png](../images/modern/11.appearanceoption.png)

