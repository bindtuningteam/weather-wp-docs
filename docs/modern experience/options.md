### Default City

Define which city users should see by default. Leaving this blank will default to **New York**.

----

### Weather Provider

Select the weather provider **OpenWeather** - you can get your key from  - <a href="https://openweathermap.org/appid" target="_blank">https://openweathermap.org/appid</a> 

----

### Provider API Key
This field is where you should insert the API Key for the weather provider you're using. 
You can aquire a key for each provider by navigating to the links in the section above.

Results from the APIs are cached to prevent the API limits from being reached.

![advanced.PNG](../images/modern/03.provideroptions.png)