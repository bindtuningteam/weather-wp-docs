### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.
 
![02.appearance.text.png](../images/modern/02.appearance.text.png)

---

### Web Part Errors

Decide how you would like the web part errors to be displayed. Three options are available

- **Show In Edit Mode Only** - Error messages will be visible only if the page is in edit mode.
- **Always Show** - Error messages will always be visible to any user.
- **Never Show** - Error messages will never be visible.

----
### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.


![Add section](../images/modern/04.advanced.png)