1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **Weather**, you can skip this process. 

    ![setting_edit.png](../images/msteams/setting_edit.png)

2. Configure the web part according to the settings described in the **[Web Part Properties](./general.md)**;
3. The properties are saved automatically, close the configuration panel.