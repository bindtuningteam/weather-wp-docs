On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Provider Options](./options.md)
- [Layout Settings](./layout.md)
- [Advanced Options](./advanced.md)
 
![06.options](../images/modern/06.options.png)